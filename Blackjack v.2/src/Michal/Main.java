package Michal;

import java.util.Scanner;

public class Main {

    private static Scanner userInput = new Scanner(System.in);

    public static void main(String[] args) {

        Deck playingDeck = new Deck();
        playingDeck.createDeck();
        //System.out.println(playingDeck);
        playingDeck.shuffle();

        double playerMoney = 100.0;

        Deck playerCards = new Deck();

        Deck croupierCards = new Deck();

        System.out.println("Let the Blackjack game begin!");


        while (playerMoney > 0) {
            System.out.println("You have: " + playerMoney + "$, how much do you want to bet");
            double playerBet = userInput.nextDouble();
            boolean endRound = false;
            if (playerBet > playerMoney) {
                System.out.println("You don't have that much money");
                break;
            }

            System.out.println("######################################");
            System.out.println("####  DRAWING 2 CARDS FOR PLAYER  ####");
            System.out.println("######################################");
            System.out.println();
            System.out.println();
            playerCards.draw(playingDeck);
            playerCards.draw(playingDeck);

            System.out.println("######################################");
            System.out.println("#### DRAWING 2 CARDS FOR CROUPIER ####");
            System.out.println("######################################");
            System.out.println();
            System.out.println();

            croupierCards.draw(playingDeck);
            croupierCards.draw(playingDeck);

            while (true) {

                System.out.println("Player hand: " + playerCards.toString());
                System.out.println("The value of players hand: " + playerCards.cardsValue());
                System.out.println("========================");
                System.out.println("========================");
                System.out.println("========================");
                System.out.println("Croupier hand: " + croupierCards.getCard(0).toString() + " and hidden");
                System.out.println();
                System.out.println("===========================");
                System.out.println("=======PLAYERS TURN========");
                System.out.println("===========================");
                System.out.println();

                System.out.println("Player, do you want to draw another card? If yes press 1, if no press 2");

                int choice = userInput.nextInt();

                if (choice == 1) {
                    playerCards.draw(playingDeck);
                    System.out.print("Player decided to draw another card and it is: " + playerCards.getCard(playerCards.mainDeckSize() - 1).toString() + ". ");

                    if (playerCards.cardsValue() > 21) {
                        System.out.println("The value is over 21. Player lose with value: " + playerCards.cardsValue());
                        playerMoney -= playerBet;
                        endRound = true;
                        break;
                    }

                } else if (choice == 2) {
                    System.out.println("Player decided not to draw anymore. Final score is: " + playerCards.cardsValue());
                    break;
                }
            }


            System.out.println("Croupier cards: " + croupierCards.toString());

            if ((croupierCards.cardsValue() >= playerCards.cardsValue()) && endRound == false) {
                System.out.println("Croupier beats player. ");
                System.out.println("Croupier value: " + croupierCards.cardsValue() + ", player value: " + playerCards.cardsValue());
                playerMoney -= playerBet;
                endRound = true;
            }

            while ((croupierCards.cardsValue() < 17) && endRound == false) {
                System.out.println("===========================");
                System.out.println("======Croupiers turn=======");
                System.out.println("===========================");
                System.out.println();
                croupierCards.draw(playingDeck);
                System.out.println("Croupier  decided to draw another card and it is: " + croupierCards.getCard(croupierCards.mainDeckSize() - 1).toString());
            }

            System.out.println("Croupier hand value: " + croupierCards.cardsValue());

            if ((croupierCards.cardsValue() > 21) && endRound == false) {
                System.out.println("Croupiers hand value is over 21."); //Final value: " + croupierCards.cardsValue());
                System.out.println("Player wins");
                playerMoney += playerBet;
                endRound = true;
            }
            /*if((croupierCards.cardsValue() == playerCards.cardsValue()) && endRound == false){
                System.out.println("Croupier card value: " + croupierCards.cardsValue() + ", player card value: " + playerCards.cardsValue());
                System.out.println("It's a draw. Croupier wins.");
                playerMoney -= playerBet;
                endRound = true;
            }*/

            if ((playerCards.cardsValue() > croupierCards.cardsValue()) && endRound == false) {
                System.out.println("Croupier card value: " + croupierCards.cardsValue() + ", player card value: " + playerCards.cardsValue());
                System.out.println("Player wins.");
                playerMoney += playerBet;
                endRound = true;
            } else if ((croupierCards.cardsValue() == playerCards.cardsValue()) && endRound == false) {
                System.out.println("Croupier card value: " + croupierCards.cardsValue() + ", player card value: " + playerCards.cardsValue());
                System.out.println("It's a draw. Croupier wins.");
                playerMoney -= playerBet;
                endRound = true;
            } else if (endRound == false) {
                System.out.println("Croupier card value: " + croupierCards.cardsValue() + ", player card value: " + playerCards.cardsValue());
                System.out.println("Croupier wins.");
                playerMoney -= playerBet;
                endRound = true;
            }

            playerCards.returnAllCardsToDeck(playingDeck);
            croupierCards.returnAllCardsToDeck(playingDeck);
            System.out.println("End of Hand.");

        }
        System.out.println("Game over.");


    }
}

