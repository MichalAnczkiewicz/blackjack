package Michal;

/**
 * Created by Michał on 12.03.2017.
 */
public class Card {
    private Suit suit;
    private Value value;

    public Card(Suit suit, Value value){
        this.suit = suit;
        this.value = value;
    }

    public String toString(){
        return this.suit.toString() + " " + this.value.toString();
    }

    public Value getValue(){
        return this.value;
    }
}
