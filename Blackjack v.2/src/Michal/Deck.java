package Michal;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Michał on 12.03.2017.
 */
public class Deck {
    /**
     * Array list that contains cards
     */
    private ArrayList<Card> cards;

    /**
     * Constructor of Deck object
     */
    public Deck() {
        this.cards = new ArrayList<Card>();
    }

    /**
     * Creating deck - 4 suits, every with 13 cards
     */
    public void createDeck() {
        for (Suit cardSuit : Suit.values()) {
            for (Value cardValue : Value.values()) {
                this.cards.add(new Card(cardSuit, cardValue));
            }
        }
    }

    /**
     * void shuffle()
     * creating a temporary deck by adding one random card from original deck
     * and then removing this card from original deck
     */

    /**
     * Specifies the value of all cards
     *
     * @return totalValue
     */
    public int cardsValue() {
        int totalValue = 0;
        int aces = 0;
        //For every card in the deck
        for (Card aCard : this.cards) {
            //Switch of possible values
            switch (aCard.getValue()) {
                case TWO:
                    totalValue += 2;
                    break;
                case THREE:
                    totalValue += 3;
                    break;
                case FOUR:
                    totalValue += 4;
                    break;
                case FIVE:
                    totalValue += 5;
                    break;
                case SIX:
                    totalValue += 6;
                    break;
                case SEVEN:
                    totalValue += 7;
                    break;
                case EIGHT:
                    totalValue += 8;
                    break;
                case NINE:
                    totalValue += 9;
                    break;
                case TEN:
                    totalValue += 10;
                    break;
                case JACK:
                    totalValue += 10;
                    break;
                case QUEEN:
                    totalValue += 10;
                    break;
                case KING:
                    totalValue += 10;
                    break;
                case ACE:
                    aces += 1;
                    break;
            }
        }

        for (int i = 0; i < aces; i++) {
            if (totalValue > 10) {
                totalValue += 1;
            } else {
                totalValue += 11;
            }
        }
        return totalValue;
    }

    public void shuffle() {
        ArrayList<Card> deck = new ArrayList<Card>();
        Random random = new Random();
        int randomCard = 0;
        int originalSize = this.cards.size();

        for (int i = 0; i < originalSize; i++) {
            //random.nextInt((max - min) + 1) + min;
            randomCard = random.nextInt((this.cards.size() - 1) + 1) + 0;
            deck.add(this.cards.get(randomCard));
            this.cards.remove(randomCard);
        }
        this.cards = deck;
    }

    /**
     * Removing card by index from deck
     *
     * @param i
     */
    public void removeCard(int i) {
        this.cards.remove(i);
    }

    /**
     * Getter method
     *
     * @param i
     * @return card with specified index
     */
    public Card getCard(int i) {
        return this.cards.get(i);
    }

    /**
     * Adding card to the deck
     *
     * @param addCard
     */
    public void addCard(Card addCard) {
        this.cards.add(addCard);
    }

    /**
     * Drawing a card which is on top in the deck and then removing it from an array
     *
     * @param topCard
     */
    public void draw(Deck topCard) {
        this.cards.add(topCard.getCard(0));
        topCard.removeCard(0);
    }


    /**
     * Making string out of card values
     *
     * @return cardListOutput
     */
    public String toString() {
        String cardListOutput = "";

        for (Card aCard : this.cards) {
            cardListOutput += "\n" + aCard.toString();
        }
        return cardListOutput;
    }


    /**
     * Putting cards into returnToDeck deck
     *
     * @param returnToDeck
     */
    public void returnAllCardsToDeck(Deck returnToDeck) {
        int deckSize = this.cards.size();
        for (int i = 0; i < deckSize; i++) {
            returnToDeck.addCard(this.getCard(i));
        }
        for (int i = 0; i < deckSize; i++) {
            this.removeCard(0);
        }
    }

    /**
     * @return the size of deck
     */
    public int mainDeckSize() {
        return this.cards.size();
    }


}
