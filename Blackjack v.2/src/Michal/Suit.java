package Michal;

/**
 * Created by Michał on 12.03.2017.
 */
public enum Suit {

    CLUB, DIAMOND, SPADE, HEART
}
